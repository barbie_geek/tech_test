
export interface Hero {
    id: number;
    name: string;
    testValues?: number[];
    testValues2?: {
        [key: string]: number;
    };
    testValues3?: {
        name: string;
        value: number;
    }[];
}

export interface CryptoCurrency {
    [fromCurrency: string]: {
        [toCurrency: string]: number;
    };
}
