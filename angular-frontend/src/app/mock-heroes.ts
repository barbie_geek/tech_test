import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Narco', testValues: [ 69, 666 ], testValues2: { hello: 666 }, testValues3: [ { name: "hello", value: 666 }, { name: "world", value: 69 } ] },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas', testValues2: { hello: 666, world: 69 } },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan', testValues3: [ { name: "hello", value: 666 }, { name: "world", value: 69 } ] },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];