import { Component, OnInit } from '@angular/core';
import { CryptoCurrency, Hero } from '../hero';
// import { HEROES } from '../mock-heroes';

import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  // hero: Hero = {
  //   id: 1,
  //   name: 'Windstorm'
  // };

  // heroes = HEROES;
  // heroes: Hero[] = [];
  // selectedHero?: Hero;
  currencies: CryptoCurrency[] = [];
  // selectedCurrency?: CryptoCurrency;
  selectedCurrency?: string;

  // constructor() { }
  constructor(private heroService: HeroService, private messageService: MessageService) {}

  ngOnInit() {
    this.getHeroes();
  }

  // onSelect(hero: Hero): void {
  //   this.selectedHero = hero;
  //   this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}`);
  // onSelect(currency: CryptoCurrency): void {
  onSelect(currency: string): void {
      this.selectedCurrency = currency;
    // this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}`);
    this.messageService.add(`HeroesComponent: Selected currency ${currency}`);
  }

  getHeroes(): void {
    // this.heroes = this.heroService.getHeroes();
    // this.heroService.getHeroes()
    //   .subscribe(heroes => this.heroes = heroes);

    this.heroService.getCurrencies()
      .subscribe(currencies => this.currencies = currencies);
  }
}
