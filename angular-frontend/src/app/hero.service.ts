import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { MessageService } from './message.service';

import { Hero, CryptoCurrency } from './hero';
// import { HEROES } from './mock-heroes';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private heroesUrl = 'http://127.0.0.1:3000/heroes'; // URL to web api
  private pricesUrl = 'http://127.0.0.1:3000/prices'; // URL to web api

  constructor(private http: HttpClient, private messageService: MessageService) { }

  getHeroes(): Observable<Hero[]> {
    // const heroes = of(HEROES);
    const heroes = this.http.get<Hero[]>(this.heroesUrl);
    this.messageService.add('HeroService: fetched heroes');
    return heroes;
  }

  getCurrencies(): Observable<CryptoCurrency[]> {
    // const heroes = of(HEROES);
    const prices = this.http.get<CryptoCurrency[]>(this.pricesUrl);
    this.messageService.add('HeroService: fetched prices');
    return prices;
  }
}
