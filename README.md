# tech_test

Real-time crypto prices using sockets + nestjs + angular

Task: Create a client/server that provides a real time crytpo currency stream for the following coins [BTC,ETH,XRP,LTC,BCH,ETC] and there value in the following currencies [USD,GBP,EUR,JPY,ZAR].

The project must be hosted on gitlab.com and https://www.cryptocompare.com must be used as the data source.

Requirements:
* Server NestJS
* Client Angular
   * gave up that part
      * was taking too long
      * I'm not a frontend/fullstack dev
      * stopped here after trying to show the prices
* Project must be hosted on gitlab.com

Nice to haves
* History
* Filters
* Tests
* WebSockets
