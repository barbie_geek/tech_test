import { Injectable } from '@nestjs/common';

import * as requestPromise from 'request-promise';

import * as config from 'config';

@Injectable()
export class PricesService {

  private _apiKey: string;
  private _fromCurrencies: string[];
  private _toCurrencies: string[];

  constructor() {
    this._apiKey = config.get<string>("apiKey");
    this._fromCurrencies = config.get<string[]>("currencies.from");
    this._toCurrencies = config.get<string[]>("currencies.to");
  }

  // getHello(): string {
  //   return 'Hello World!';
  // }

  async getPrices(): Promise<any> {

    return this.getPricesRaw(this._fromCurrencies, this._toCurrencies);
  }

  async getPricesRaw(fromCurrencies: string[], toCurrencies: string[]): Promise<any> {

    const url = `https://min-api.cryptocompare.com/data/pricemulti?fsyms=${fromCurrencies.join(",")}&tsyms=${toCurrencies.join(",")}`;

    const opts: requestPromise.Options = {
      url,
      headers: {
        authorization: `Apikey ${this._apiKey}`
      }
    };

    const response = await requestPromise.get(opts);

    // console.log(response);

    return response;
  }

  async getDaylyHistoryPrice(fromCurrency: string, toCurrency: string, limit: number, toTimestamp?: number): Promise<any> {

    let url = `https://min-api.cryptocompare.com/data/v2/histoday?fsym=${fromCurrency}&tsym=${toCurrency}&limit=${limit}`;
    if (toTimestamp)
      url += `&toTs=${toTimestamp}`;

    const opts: requestPromise.Options = {
      url,
      headers: {
        authorization: `Apikey ${this._apiKey}`
      }
    };

    const response = await requestPromise.get(opts);

    // console.log(response);

    return response;
  }

  // https://min-api.cryptocompare.com/data/v2/histoday?fsym=BTC&tsym=USD&limit=10&toTs=1612051200
}
