import { BadRequestException, Controller, Get, HttpException, Param, Query } from '@nestjs/common';
import { PricesService } from '../services/prices.service';

@Controller()
export class PricesController {
  constructor(private readonly pricesService: PricesService) {}

  // @Get()
  // getHello(): string {
  //   return this.pricesService.getHello();
  // }


  @Get("prices")
  async getRequiredPrices(): Promise<any> {
    return await this.pricesService.getPrices();
  }

  @Get("custom_prices")
  async getCustomPrices(
    @Query('fromCurrencies') fromCurrencies: string,
    @Query('toCurrencies') toCurrencies: string,
  ): Promise<any> {

    if (!fromCurrencies)
      throw new BadRequestException('missing fromCurrencies query value');

    if (!toCurrencies)
      throw new BadRequestException('missing toCurrencies query value');

    const fromValues = fromCurrencies.split(",").filter(item => item.length > 0);
    if (fromValues.length === 0)
      throw new BadRequestException('empty fromCurrencies query value');

    const toValues = toCurrencies.split(",").filter(item => item.length > 0);
    if (toValues.length === 0)
      throw new BadRequestException('empty toCurrencies query value');

    return await this.pricesService.getPricesRaw(fromValues, toValues);
  }

  @Get("history_prices")
  async getHistoryPrices(
    @Query('fromCurrency') fromCurrency: string,
    @Query('toCurrency') toCurrency: string,
    @Query('limit') limit: string,
    @Query('toTimestamp') toTimestamp?: string,
  ): Promise<any> {

    if (!fromCurrency)
      throw new BadRequestException('missing fromCurrency query value');

    if (!toCurrency)
      throw new BadRequestException('missing toCurrency query value');

    if (!limit)
      throw new BadRequestException('missing limit query value');

    const limitValue = parseInt(limit, 10);
    if (isNaN(limitValue) || limitValue < 10)
      throw new BadRequestException('invalid limit query value');

    let toTimestampValue: number = undefined;
    if (toTimestamp) {
      toTimestampValue = parseInt(toTimestamp, 10);
      if (isNaN(toTimestampValue) || toTimestampValue < 0)
        throw new BadRequestException('invalid toTimestamp query value');
    }

    return await this.pricesService.getDaylyHistoryPrice(fromCurrency, toCurrency, limitValue);
  }

  // @Get("heroes")
  // async getHeroes(): Promise<any> {
  //   return [
  //     { id: 11, name: 'Dr Nice' },
  //     { id: 12, name: 'Narco', testValues: [ 69, 666 ], testValues2: { hello: 666 }, testValues3: [ { name: "hello", value: 666 }, { name: "world", value: 69 } ] },
  //     { id: 13, name: 'Bombasto' },
  //     { id: 14, name: 'Celeritas', testValues2: { hello: 666, world: 69 } },
  //     { id: 15, name: 'Magneta' },
  //     { id: 16, name: 'RubberMan', testValues3: [ { name: "hello", value: 666 }, { name: "world", value: 69 } ] },
  //     { id: 17, name: 'Dynama' },
  //     { id: 18, name: 'Dr IQ' },
  //     { id: 19, name: 'Magma' },
  //     { id: 20, name: 'Tornado' }
  //   ];
  // }

}
