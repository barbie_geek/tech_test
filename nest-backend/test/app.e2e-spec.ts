import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  // it('/ (GET)', () => {
  //   return request(app.getHttpServer())
  //     .get('/')
  //     .expect(200)
  //     .expect('Hello World!');
  // });

  it('/prices (GET)', () => {
    return request(app.getHttpServer())
      .get('/prices')
      .expect(200);
  });

  describe('GET /custom_prices', () => {

    it('success', () => {
      return request(app.getHttpServer())
        .get('/custom_prices?fromCurrencies=BTC,ETH,XRP,LTC,BCH,ETC&toCurrencies=USD,GBP,EUR,JPY,ZAR')
        .expect(200);
    });

    it('invalid query (missing fromCurrencies)', () => {
      return request(app.getHttpServer())
        .get('/custom_prices?toCurrencies=USD,GBP,EUR,JPY,ZAR')
        .expect(400);
    });

    it('invalid query (missing toCurrencies)', () => {
      return request(app.getHttpServer())
        .get('/custom_prices?fromCurrencies=BTC,ETH,XRP,LTC,BCH,ETC')
        .expect(400);
    });

    it('invalid query (empty fromCurrencies)', () => {
      return request(app.getHttpServer())
        .get('/custom_prices?fromCurrencies=')
        .expect(400);
    });

    it('invalid query (empty fromCurrencies)', () => {
      return request(app.getHttpServer())
        .get('/custom_prices?fromCurrencies=,,,,')
        .expect(400);
    });

    it('invalid query (empty toCurrencies)', () => {
      return request(app.getHttpServer())
        .get('/custom_prices?fromCurrencies=BTC,ETH,XRP,LTC,BCH,ETC&toCurrencies=')
        .expect(400);
    });

    it('GET /custom_prices invalid query (empty toCurrencies)', () => {
      return request(app.getHttpServer())
        .get('/custom_prices?fromCurrencies=BTC,ETH,XRP,LTC,BCH,ETC&toCurrencies=,,,,')
        .expect(400);
    });
  });

  describe('GET /history_prices', () => {

    it('success', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC&toCurrency=USD&limit=10')
        .expect(200);
    });

    it('success (with toTimestamp)', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC&toCurrency=USD&limit=10&toTimestamp=1612051200')
        .expect(200);
    });

    it('invalid query (missing fromCurrency)', () => {
      return request(app.getHttpServer())
        .get('/history_prices')
        .expect(400);
    });

    it('invalid query (missing toCurrency)', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC')
        .expect(400);
    });

    it('invalid query (missing limit)', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC&toCurrency=USD')
        .expect(400);
    });

    it('invalid query (NaN limit)', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC&toCurrency=USD&limit=LOL')
        .expect(400);
    });

    it('invalid query (negative limit)', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC&toCurrency=USD&limit=-666')
        .expect(400);
    });

    it('invalid query (NaN toTimestamp)', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC&toCurrency=USD&limit=10&toTimestamp=LOL')
        .expect(400);
    });

    it('invalid query (negative toTimestamp)', () => {
      return request(app.getHttpServer())
        .get('/history_prices?fromCurrency=BTC&toCurrency=USD&limit=10&toTimestamp=-666')
        .expect(400);
    });
  });

});
